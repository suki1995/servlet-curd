<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Student Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a Student
</h1>



<form action="Admin" method="post">
	<center>
	<table border="1">
	<tr>
		<td>
			Name  :<input type="text" name="name" value="Suki">

		</td>
		</tr>
		<tr>
		<td>
			Age  :<input type="text" name="age" value="22">

		</td> 
		
		</tr>
		<tr>
	
		<td>
			Mobile  :<input type="text" name="mobile" value="1111111111">

		</td>

		</tr>
		<tr>
		<td>
		Mail  :<input type="text" name="mail" value="suki@gmail.com">

		</td> 
	</tr>
	<tr>
		<td>
			<input type="submit" name="action" value="Add Student">
		</td> 
	</tr>
</table>

</form>

<form action="Admin" method="post">
	<center>
	<c:forEach var="single" items="${student}" >
	<table border="1">
	<tr>
		<td>
			Id  :<input type="text" name="id" value="${single.id }">

		</td>
		</tr>
	<tr>
		<td>
			Name  :<input type="text" name="name" value="${single.name }">

		</td>
		</tr>
		<tr>
		<td>
			Age  :<input type="text" name="age" value="${single.age }">

		</td> 
		
		</tr>
		<tr>
	
		<td>
			Mobile  :<input type="text" name="mobile" value="${single.mobile }">

		</td>

		</tr>
		<tr>
		<td>
		Mail  :<input type="text" name="mail" value="${single.mail }">

		</td> 
	</tr>
	<tr>
		<td>
			<input type="submit" name="action" value="Update Student">
		</td> 
	</tr>
</table>
</c:forEach>
</form>

<h3>Persons List</h3>
	<table class="tg">
	<tr>
		<th width="80"> ID</th>
		<th width="120"> Name</th>
		<th width="120">Age</th>
		<th width="60">Mobile</th>
		<th width="60">Mail</th>
		<th width="60">Action</th>
	</tr>
	<c:forEach var="student" items="${studentlist}" >
		<tr>
			<td>${student.id}</td>
			<td>${student.name}</td>
			<td>${student.age}</td>
						<td>${student.mobile}</td>
			
						<td>${student.mail}</td>
			
			<td><a href="Admin?action=Student Update&Id=<c:out value="${student.id }"/>">Update</a>
		<a href="Admin?action=Student Delete&Id=<c:out value="${student.id }"/>">Delete</a></td>
		</tr>
	</c:forEach>
	</table>
	</center>
		
</body>
</html>
