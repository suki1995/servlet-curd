package com.aeq.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.aeq.connection.DbConnection;
import com.aeq.domain.Student;

public class StudentDao {

 
public void insert(Student student) 
{
	try{
	Connection con=DbConnection.getConnection();
	PreparedStatement ps=con.prepareStatement("insert into student values(?,?,?,?,?)");

ps.setInt(1,student.getId());
ps.setString(2,student.getName());
ps.setInt(3,student.getAge());
ps.setLong(4,student.getMobile());
ps.setString(5, student.getMail());
ps.executeUpdate();

} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
}

public void delete(int id) 
{
try{
Connection con=DbConnection.getConnection();
PreparedStatement ps=con.prepareStatement("delete from student where id=?");
ps.setInt(1, id);
ps.executeUpdate();
} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
}
public List<Student> view() 
{
	List<Student> list=new ArrayList<Student>();

	try {
	Connection con = DbConnection.getConnection();
	PreparedStatement 	ps = con.prepareStatement("select * from student");

	
	
	ResultSet rs = ps.executeQuery();
	while (rs.next()) 
	{
		Student student=new Student();
		
		student.setId(rs.getInt(1));
		student.setName(rs.getString(2));
		student.setAge(rs.getInt(3));
		student.setMobile(rs.getLong(4));
		student.setMail(rs.getString(5));
		list.add(student);
	}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return list;
	
}
public void edit(Student student)
{
	try{
		Connection con=DbConnection.getConnection();
		PreparedStatement ps=con.prepareStatement("update student set name=?, age=?, mobile=?, mail=? where id=?");
		
		ps.setString(1, student.getName());
		ps.setInt(2, student.getAge());
		ps.setLong(3, student.getMobile());
		ps.setString(4,student.getMail());
		ps.setInt(5,student.getId());
		ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
}

public List<Student> getById(int updateid) {
	
	List<Student> list=new ArrayList<Student>();

	try {
	Connection con = DbConnection.getConnection();
	PreparedStatement 	ps = con.prepareStatement("select * from student where id=?");

	ps.setInt(1, updateid);
	
	ResultSet rs = ps.executeQuery();
	while (rs.next()) 
	{
		Student student=new Student();
		
		student.setId(rs.getInt(1));
		student.setName(rs.getString(2));
		student.setAge(rs.getInt(3));
		student.setMobile(rs.getLong(4));
		student.setMail(rs.getString(5));
		list.add(student);
	}
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return list;
	
}
}
