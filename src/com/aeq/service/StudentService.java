package com.aeq.service;

import java.util.List;

import com.aeq.domain.Student;

public interface StudentService {
	
	public void add(Student student);
	public void remove(int id);
	public List<Student> display();
	public void edit(Student student);

}
