package com.aeq.service;

import java.util.List;

import com.aeq.dao.StudentDao;
import com.aeq.domain.Student;

public class StudentServiceImp implements StudentService  {
	StudentDao dao=new StudentDao();

	@Override
	public void add(Student student) {
		
		dao.insert(student);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remove(int id) {
		dao.delete(id);
		// TODO Auto-generated method stub
		
	}
		@Override
	public List<Student> display() {

		
		return dao.view();
	}

		@Override
		public void edit(Student student) {
		    dao.edit(student);
			
		}

		public List<Student> getUpdateValue(int updateid) {
			// TODO Auto-generated method stub
			return dao.getById(updateid);
		}

}
