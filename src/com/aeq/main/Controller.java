package com.aeq.main;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aeq.domain.Student;
import com.aeq.service.StudentServiceImp;



@WebServlet("/Controller")
public class Controller extends HttpServlet {

	private static final long serialVersionUID = 1L;
    
	   
	protected void doGet(HttpServletRequest req, HttpServletResponse resp ) throws ServletException, IOException 
	{
		
		StudentServiceImp ssi=new StudentServiceImp();
		
		String option=req.getParameter("action");
		
			if(option.equalsIgnoreCase("home"))
			{
				req.setAttribute("studentlist", ssi.display());
			
				RequestDispatcher rd=req.getRequestDispatcher("student.jsp");
				rd.forward(req, resp);
				
			}
			
			else if(option.equalsIgnoreCase("Student Delete"))
			{
				int id=Integer.parseInt(req.getParameter("Id"));
				ssi.remove(id);
				req.setAttribute("studentlist", ssi.display());
						
				RequestDispatcher rd=req.getRequestDispatcher("student.jsp");
					rd.forward(req, resp);
			}
			else if(option.equalsIgnoreCase("Student Update"))
			{
				int updateid=Integer.parseInt(req.getParameter("Id"));
				
				req.setAttribute("student", ssi.getUpdateValue(updateid));
				req.setAttribute("sample", ssi.getUpdateValue(updateid));
				req.setAttribute("studentlist", ssi.display());
				
				RequestDispatcher rd=req.getRequestDispatcher("student.jsp");
					rd.forward(req, resp);

			}



	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		StudentServiceImp ssi=new StudentServiceImp();

		 String option=req.getParameter("action");
		if(option.equalsIgnoreCase("Add Student"))
		{
			Student student=new Student();
			student.setName(req.getParameter("name"));
			student.setAge(Integer.parseInt(req.getParameter("age")));
			student.setMobile(Long.parseLong(req.getParameter("mobile")));
			student.setMail(req.getParameter("mail"));
			ssi.add(student);
			

			req.setAttribute("studentlist", ssi.display());
		
			RequestDispatcher rd=req.getRequestDispatcher("student.jsp");
			rd.forward(req, resp);
			
		}
		if(option.equalsIgnoreCase("Update Student"))
		{
			Student student=new Student();
			student.setId(Integer.parseInt(req.getParameter("id")));

			student.setName(req.getParameter("name"));
			student.setAge(Integer.parseInt(req.getParameter("age")));
			student.setMobile(Long.parseLong(req.getParameter("mobile")));
			student.setMail(req.getParameter("mail"));
			ssi.edit(student);
			

			req.setAttribute("studentlist", ssi.display());
		
			RequestDispatcher rd=req.getRequestDispatcher("student.jsp");
			rd.forward(req, resp);
			
		}
		 
	}

}
